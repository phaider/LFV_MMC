#include "TGraph.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TLegend.h"
#include "TCanvas.h"

void METPlots() {

  // for loop to run two times with different data?

  TFile *data = new TFile("./SmearedCorrectedMETLFV.root");
//  vector<TString> endings = {};
  vector<TString> endings = {".root", ".png", ".pdf", ".eps"};

  TString saveOption = "";
  TCanvas *canvas = new TCanvas("canvas", "canvas1");
  canvas->Clear();

  TH1F *DeltaMETXSmearTrue         = (TH1F*)data->Get("DeltaMETXSmearTrue");
  TH1F *DeltaMETModSmearTrue       = (TH1F*)data->Get("DeltaMETModSmearTrue");
  TH1F *DeltaMETXGammaCorrTrue     = (TH1F*)data->Get("DeltaMETXGammaCorrTrue");
  TH1F *DeltaMETXFitCorrTrue       = (TH1F*)data->Get("DeltaMETXFitCorrTrue");
  TH1F *DeltaMETModGammaCorrTrue   = (TH1F*)data->Get("DeltaMETModGammaCorrTrue");
  TH1F *DeltaMETModFitCorrTrue     = (TH1F*)data->Get("DeltaMETModFitCorrTrue");
  TH1F *DeltaMETXGammaCorrSmear    = (TH1F*)data->Get("DeltaMETXGammaCorrSmear");
  TH1F *DeltaMETXFitCorrSmear      = (TH1F*)data->Get("DeltaMETXFitCorrSmear");
  TH1F *DeltaMETModGammaCorrSmear  = (TH1F*)data->Get("DeltaMETModGammaCorrSmear");
  TH1F *DeltaMETModFitCorrSmear    = (TH1F*)data->Get("DeltaMETModFitCorrSmear");

  DeltaMETXSmearTrue               -> SetLineColor(kBlue);
  DeltaMETModSmearTrue             -> SetLineColor(kBlue);
  DeltaMETXGammaCorrTrue           -> SetLineColor(kRed);
  DeltaMETModGammaCorrTrue         -> SetLineColor(kRed);
  DeltaMETXGammaCorrSmear          -> SetLineColor(kGreen);
  DeltaMETModGammaCorrSmear        -> SetLineColor(kGreen);
  DeltaMETXFitCorrTrue             -> SetLineColor(kMagenta);
  DeltaMETXFitCorrSmear            -> SetLineColor(kCyan+1);
  DeltaMETModFitCorrTrue           -> SetLineColor(kBlue);
  DeltaMETModFitCorrSmear          -> SetLineColor(kOrange+3);

  double width = DeltaMETXSmearTrue->GetBinWidth(1);
  DeltaMETXGammaCorrTrue->SetTitle(Form("E_{T}^{miss} resolution;#Delta E_{T,x}^{miss} / GeV; Ereignisse / %'.1f GeV", width)) ;
  DeltaMETXGammaCorrTrue->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETXGammaCorrTrue           -> DrawNormalized();
//  DeltaMETXFitCorrTrue             -> Draw("same");
  DeltaMETXSmearTrue               -> DrawNormalized("same");

  TLegend *leg1 = new TLegend(0.65,0.75,0.92,0.92);
  leg1->AddEntry(DeltaMETXSmearTrue           ,"nach Verschmierung","l");
  leg1->AddEntry(DeltaMETXGammaCorrTrue       ,"nach Korrektur","l");
//  leg1->AddEntry(DeltaMETXSmearTrue           ,"E_{T,x,smear}^{miss} - E_{T,x,true}^{miss}","l");
//  leg1->AddEntry(DeltaMETXGammaCorrTrue       ,"E_{T,x,corr}^{miss} - E_{T,x,true}^{miss} (#Gamma_{S})","l");
//  leg1->AddEntry(DeltaMETXFitCorrTrue         ,"E_{T,x,corr}^{miss} - E_{T,x,true}^{miss} (Fit)","l");

  leg1->Draw();
  canvas->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaMETX";
      saveName += saveOption;
      saveName.Append(ftype);
      canvas->SaveAs(saveName);
    }
  }

  TCanvas *c2 = new TCanvas("c2", "canvas2");
  c2->Clear();

  width = DeltaMETModSmearTrue->GetBinWidth(1);
  DeltaMETModGammaCorrTrue->SetTitle(Form("E_{T}^{miss} resolution;#Delta |E_{T}^{miss}| / GeV; Ereignisse / %'.1f GeV", width)) ;
  DeltaMETModGammaCorrTrue->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETModSmearTrue             -> DrawNormalized();
//  DeltaMETModFitCorrTrue           -> Draw("same");
  DeltaMETModGammaCorrTrue         -> DrawNormalized("same");

  TLegend *leg2 = new TLegend(0.65,0.75,0.92,0.92);
  leg2->AddEntry(DeltaMETModSmearTrue         ,"nach Verschmierung","l");
  leg2->AddEntry(DeltaMETModGammaCorrTrue     ,"nach Korrektur","l");
//  leg2->AddEntry(DeltaMETModSmearTrue         ,"|E_{T,smear}^{miss}| - |E_{T,x,true}^{miss}|","l");
//  leg2->AddEntry(DeltaMETModGammaCorrTrue     ,"|E_{T,corr}^{miss}| - |E_{T,true}^{miss}| (#Gamma_{S})","l");
//  leg2->AddEntry(DeltaMETModFitCorrTrue       ,"|E_{T,corr}^{miss}| - |E_{T,true}^{miss}| (Fit)","l");

  leg2->Draw();
  c2->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaMETMod";
      saveName += saveOption;
      saveName.Append(ftype);
      c2->SaveAs(saveName);
    }
  }

  TCanvas *c3 = new TCanvas("c3", "canvas3");
  c3->Clear();

  width = DeltaMETXGammaCorrSmear->GetBinWidth(1);
  DeltaMETXGammaCorrSmear->SetTitle(Form("E_{T}^{miss} resolution;#Delta E_{T}^{miss} / GeV; Counts / %'.1f GeV", width)) ;
  DeltaMETXGammaCorrSmear->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETXGammaCorrSmear          -> Draw();
//  DeltaMETXFitCorrSmear            -> Draw("same");

  TLegend *leg3 = new TLegend(0.65,0.7,0.92,0.92);
  leg3->AddEntry(DeltaMETXGammaCorrSmear      ,"E_{T,x,corr}^{miss} - E_{T,x,smear}^{miss} (#Gamma_{S})","l");
//  leg3->AddEntry(DeltaMETXFitCorrSmear        ,"E_{T,x,corr}^{miss} - E_{T,x,smear}^{miss} (Fit)","l");
  leg3->Draw();
  c3->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaXCorrSmear";
      saveName += saveOption;
      saveName.Append(ftype);
      c3->SaveAs(saveName);
    }
  }

  TCanvas *c4 = new TCanvas("c4", "canvas4");
  c4->Clear();

  width = DeltaMETModGammaCorrSmear->GetBinWidth(1);
  DeltaMETModGammaCorrSmear->SetTitle(Form("E_{T}^{miss} resolution;#Delta E_{T}^{miss} / GeV; Counts / %'.1f GeV", width)) ;
  DeltaMETModGammaCorrSmear->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETModGammaCorrSmear        -> Draw();
//  DeltaMETModFitCorrSmear          -> Draw("same");

  TLegend *leg4 = new TLegend(0.65,0.7,0.92,0.92);
  leg4->AddEntry(DeltaMETModGammaCorrSmear    ,"|E_{T,corr}^{miss}| - |E_{T,smear}^{miss}| (#Gamma_{S})","l");
//  leg4->AddEntry(DeltaMETModFitCorrSmear      ,"|E_{T,corr}^{miss}| - |E_{T,smear}^{miss}| (Fit)","l");
  leg4->Draw();
  c4->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaModCorrSmear";
      saveName += saveOption;
      saveName.Append(ftype);
      c4->SaveAs(saveName);
    }
  }

  // graphs for correction without smear
  data = new TFile("./CorrectedMETLFV.root");

  DeltaMETXGammaCorrTrue      = (TH1F*)data->Get("DeltaMETXGammaCorrTrue");
//  DeltaMETXFitCorrTrue        = (TH1F*)data->Get("DeltaMETXFitCorrTrue");
  DeltaMETModGammaCorrTrue    = (TH1F*)data->Get("DeltaMETModGammaCorrTrue");
//  DeltaMETModFitCorrTrue      = (TH1F*)data->Get("DeltaMETModFitCorrTrue");
  TH2F *DeltaMETXYCorrTrue    = (TH2F*)data->Get("DeltaMETXYCorrTrue");
  TH2F *DeltaMETXModCorrTrue  = (TH2F*)data->Get("DeltaMETXModCorrTrue");
  TH2F *ScannedDeltaMETWeight = (TH2F*)data->Get("ScannedDeltaMETWeight");

  DeltaMETXGammaCorrTrue           -> SetLineColor(kRed);
//  DeltaMETXFitCorrTrue             -> SetLineColor(kMagenta);
  DeltaMETModGammaCorrTrue         -> SetLineColor(kGreen+1);
//  DeltaMETModFitCorrTrue           -> SetLineColor(kBlue);

  TCanvas *c5 = new TCanvas("c5", "canvas5");
  c5->Clear();

  width = DeltaMETXGammaCorrTrue->GetBinWidth(1);
  DeltaMETXGammaCorrTrue->SetTitle(Form("E_{T}^{miss} resolution without smearing;#Delta E_{T}^{miss} / GeV; Counts / %'.1f GeV", width)) ;
  DeltaMETXGammaCorrTrue->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETXGammaCorrTrue           -> Draw();
//  DeltaMETXFitCorrTrue             -> Draw("same");

  TLegend *leg5 = new TLegend(0.65,0.7,0.92,0.92);
  leg5->AddEntry(DeltaMETXGammaCorrTrue       ,"E_{T,x,corr}^{miss} - E_{T,x,true}^{miss} (#Gamma_{S})","l");
//  leg5->AddEntry(DeltaMETXFitCorrTrue         ,"E_{T,x,corr}^{miss} - E_{T,x,true}^{miss} (Fit)","l");

  leg5->Draw();
  c5->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaXCorrNoSmear";
      saveName += saveOption;
      saveName.Append(ftype);
      c5->SaveAs(saveName);
    }
  }

  TCanvas *c6 = new TCanvas("c6", "canvas6");
  c6->Clear();

  width = DeltaMETModGammaCorrTrue->GetBinWidth(1);
  DeltaMETModGammaCorrTrue->SetTitle(Form("E_{T}^{miss} resolution without smearing;#Delta E_{T}^{miss} / GeV; Counts / %'.1f GeV", width)) ;
  DeltaMETModGammaCorrTrue->GetYaxis()->SetTitleOffset(1.3);

  DeltaMETModGammaCorrTrue           -> Draw();
//  DeltaMETModFitCorrTrue             -> Draw("same");

  TLegend *leg6 = new TLegend(0.65,0.7,0.92,0.92);
  leg6->AddEntry(DeltaMETModGammaCorrTrue       ,"|E_{T,corr}^{miss}| - |E_{T,true}^{miss}| (#Gamma_{S})","l");
//  leg6->AddEntry(DeltaMETModFitCorrTrue         ,"|E_{T,corr}^{miss}| - |E_{T,true}^{miss}| (Fit)","l");

  leg6->Draw();
  c6->SetGrid();

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaModCorrNoSmear";
      saveName += saveOption;
      saveName.Append(ftype);
      c6->SaveAs(saveName);
    }
  }

//  // check certain m_nu fraction
//  TCanvas *c7 = new TCanvas("c7", "canvas7");
//  c7->Clear();
//
//  width = DeltaMETModGammaCorrTrue->GetBinWidth(1);
//  DeltaMETModGammaCorrTrue->SetTitle(Form("E_{T}^{miss} resolution after correction (#Gamma_{S}) without smearing;(|E_{T,corr}^{miss}| - |E_{T,true}^{miss}|) / GeV; Counts / %'.1f GeV", width));
//  DeltaMETModGammaCorrTrue->GetXaxis()->SetTitleOffset(1.1);
//  DeltaMETModGammaCorrTrue->GetYaxis()->SetTitleOffset(1.3);
//
//  DeltaMETModGammaCorrTrue           -> Draw();
//  DeltaMETModGammaCorrTrue           -> SetStats(1);
//  gStyle->SetOptFit(1011);
//
//  TLegend *leg7 = new TLegend(0.15,0.81,0.40,0.89);
//  leg7->AddEntry(DeltaMETModGammaCorrTrue       ,"1300 MeV < m_{#nu}","l");
//
//  leg7->Draw();
//  c7->SetGrid();
//
//  c7->SaveAs("./plots/met/DeltaModCorrNoSmearOnlyNuMassGreater1300.pdf");
//  c7->SaveAs("./plots/met/DeltaModCorrNoSmearOnlyNuMassGreater1300.eps");
//  c7->SaveAs("./plots/met/DeltaModCorrNoSmearOnlyNuMassGreater1300.png");
//  c7->SaveAs("./plots/met/DeltaModCorrNoSmearOnlyNuMassGreater1300.root");
//
//  cout << "Mean of Delta MET resolution   " <<  DeltaMETModGammaCorrTrue->GetMean() << endl;
//  cout << "StdDev of Delta MET resolution " << DeltaMETModGammaCorrTrue->GetStdDev() << endl;

  DeltaMETXYCorrTrue   ->GetYaxis()->SetTitleOffset(1.3);
  DeltaMETXModCorrTrue ->GetYaxis()->SetTitleOffset(1.3);

  TCanvas *c8 = new TCanvas("c8", "canvas8");
  c8->Clear();
  DeltaMETXYCorrTrue->Draw("colz");

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaMETXYCorrTrueScatter";
      saveName += saveOption;
      saveName.Append(ftype);
      c8->SaveAs(saveName);
    }
  }

  TCanvas *c9 = new TCanvas("c9", "canvas9");
  c9->Clear();
  DeltaMETXModCorrTrue->Draw("colz");

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "DeltaMETXModCorrTrueScatter";
      saveName += saveOption;
      saveName.Append(ftype);
      c9->SaveAs(saveName);
    }
  }

  TCanvas *c10 = new TCanvas("c10", "canvas10");
  c10->Clear();
  ScannedDeltaMETWeight   -> GetYaxis()->SetTitleOffset(1.5);
  ScannedDeltaMETWeight   -> SetStats(1);
  gStyle->SetOptFit(1011);
  ScannedDeltaMETWeight->Draw("colz");

  if (endings.size() > 0) {
    for (auto ftype : endings) {
      TString saveName =  "./plots/met/";
      saveName += "ScannedDeltaMETWeightDistributionScatter";
      saveName += saveOption;
      saveName.Append(ftype);
      c10->SaveAs(saveName);
    }
  }

  // graphs for correction without smear
  data = new TFile("./SmearedCorrectedMETLFV.root");

  DeltaMETXYCorrTrue               = (TH2F*)data->Get("DeltaMETXYCorrTrue");
  DeltaMETXModCorrTrue             = (TH2F*)data->Get("DeltaMETXModCorrTrue");
  TH2F *DeltaMETXYSmearTrue        = (TH2F*)data->Get("DeltaMETXYSmearTrue");
  TH2F *DeltaMETXModSmearTrue      = (TH2F*)data->Get("DeltaMETXModSmearTrue");

  vector<TH2F*> scatterPlots;
  scatterPlots.push_back(DeltaMETXYCorrTrue);
  scatterPlots.push_back(DeltaMETXModCorrTrue);
  scatterPlots.push_back(DeltaMETXModSmearTrue);
  scatterPlots.push_back(DeltaMETXYSmearTrue);

  TCanvas *c11 = new TCanvas("c11", "canvas11");
  for (auto scatter : scatterPlots) {
    c11->Clear();
    scatter -> GetYaxis()->SetTitleOffset(1.2);
    scatter -> SetStats(1);
    gStyle->SetOptFit(1011);
    scatter->Draw("colz");

    if (endings.size() > 0) {
      for (auto ftype : endings) {
        TString saveName =  "./plots/met/";
        saveName.Append(scatter->GetName());
        if (scatter == DeltaMETXYCorrTrue || scatter == DeltaMETXModCorrTrue) {
          saveName += "WithSmear";
        }
        saveName += "Scatter";
        saveName.Append(ftype);
        c11->SaveAs(saveName);
      }
    }
  }

//  delete canvas;
//  delete c2;
//  delete c3;
//  delete c4;
//  delete c5;
//  delete c6;

// ORIGINAL CODE
//  TCanvas c3("c3","canvas3");
//  // MET comparsion of smeared and corrected
//  histo42->SetTitle("resolution of the absolute value of MET;#Delta |E_{T,x}^{miss}| / GeV;event rate");
//  histo41->SetLineColor(kRed);
//  histo42->SetLineColor(kGreen);
//  histo43->SetLineColor(kBlue);
//  histo42->DrawNormalized();
////  histo41->DrawNormalized("same");
//  histo43->DrawNormalized("same");
//  TLegend *leg3 = new TLegend(0.2,0.7,0.4,0.9);
////  leg3->AddEntry(histo41,"Smeared MET","l");
//  leg3->AddEntry(histo42,"Corrected MET Matrix","l");
//  leg3->AddEntry(histo43,"Corrected MET Fit","l");
////  leg3->AddEntry(histo43,"True MET","l");
//  leg3->Draw();
////  c3.WaitPrimitive();

}
