#include "MMC.h"
#include <iostream>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TH1.h>

// CONSTRUCTOR
MMC::MMC() {
  cout << "MMC object created" << endl;
}

// VARIABLES
const double tauMass = 1776.82; //MeV/c^2

// NEW FUNCTIONS FOR KATHARINA

// if function returns true then first argument is FV lepton from Higgs decay
// and second argument is lepton from tau decay
bool MMC::leptonLabeling(TLorentzVector lep1, TLorentzVector lep2, TVector2 MET) {
  TVector2 transLep1(lep1.X(), lep1.Y());
  TVector2 transLep2(lep2.X(), lep2.Y());
  double DeltaPhi1 = TMath::Abs(MET.DeltaPhi(transLep1));
  double DeltaPhi2 = TMath::Abs(MET.DeltaPhi(transLep2));
  double poss1 = (lep1.Pt() - lep2.Pt())/1000. - MMC::fLimit(DeltaPhi1, DeltaPhi2);
  if (poss1 > 0) return 1;
  else return 0;
}

// correct MET vector by returning the corrected one
TVector2 MMC::correctMET(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double METscanPoints, double sigma ) {
  vector<double> mNuVec;
  for (double mNu = 0; mNu < 1.8e3; mNu += 1.8e3/METscanPoints) {
    mNuVec.push_back(mNu);
  }
  double TotalMETWeight= 0, TotalWeight= 0;
  TVector2 WeightedMETSum, CorrectedMET;
  // Scan over z and sigma
  for (double z = 0.01; z < 1;z += 1/METscanPoints) {
    for (double phi = 0.01; phi < 6.28; phi += 6.28/METscanPoints) {
      double DeltaX = TMath::Sqrt(-2*TMath::Log(z))*sigma*TMath::Cos(phi);
      double DeltaY = TMath::Sqrt(-2*TMath::Log(z))*sigma*TMath::Sin(phi);
      TVector2 DeltaMET(DeltaX, DeltaY);
      for (auto mNu: mNuVec) {
        double plus = MMC::calculateMassNuMScanPlus(mNu, lepton, lepFV, DeltaMET + MET);
        double weight = MMC::calculateWeightMassScan(mNu);
        // Gewichte für MET scan
        if (not TMath::IsNaN(plus)) {
          TotalWeight += weight;
        }
      }
      TotalMETWeight += TotalWeight;
      WeightedMETSum += TotalWeight*(DeltaMET + MET);
      TotalWeight = 0;
    }
  }
  if (TotalMETWeight != 0) {
    CorrectedMET = WeightedMETSum/TotalMETWeight;
  }
  return CorrectedMET;
}

// scan over m_nu
// calculate mass scan with +- mean

double MMC::massScanMMC(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints) {
  double weightedMean, m, weight;
  TH1F *histo = new TH1F("massScanHisto", "scan for single event with #Gamma_{S} weight;m_{MMC} / GeV;Counts / 0.1 GeV", 3000, -50, 250);
  for (double mNu = 0; mNu < 1.8e3; mNu += 1.8e3/scanPoints) {
    m = MMC::calculateMassNuMScan(mNu, lepton, lepFV, MET);
    weight = MMC::calculateWeightMassScan(mNu);
    histo->Fill(m/1000, weight);
  }
  weightedMean = histo->GetMean();
  delete histo;
  return weightedMean;
}

// without using a histogram
double MMC::massScanMMCWithoutHisto(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints) {
  vector<double> meanVec, weightVec;
  double weightedMean, m, weight;
  for (double mNu = 0; mNu < 1.8e3; mNu += 1.8e3/scanPoints) {
    m = MMC::calculateMassNuMScan(mNu, lepton, lepFV, MET);
    weight = MMC::calculateWeightMassScan(mNu);
    if (not TMath::IsNaN(m)) {
      meanVec.push_back(m/1000.);
      weightVec.push_back(weight);
    }
  }
  weightedMean = TMath::Mean(meanVec.begin(), meanVec.end(), weightVec.begin());
  return weightedMean;
}

// scan over energy of the neutrino system with closer Pz
double MMC::energyScanMMC(TVector2 MET, TLorentzVector lepton, TLorentzVector lepFV, double scanPoints) {
  TH1F *histo = new TH1F("energyScanHisto", "scan for single event with #Gamma_{S} weight;m_{MMC} / GeV;Counts / 0.1 GeV", 3000, -50, 250);
  double weightedMean, m, weight;
  // energy must be at least greater than MET magnitude
  for (double nuEnergy = MET.Mod(); nuEnergy < 5e5; nuEnergy += (5e5 - MET.Mod())/scanPoints) {
    m = MMC::calculateMassEnergyScanCloserPz(nuEnergy, lepton, lepFV, MET);
    vector<double> weight_res = MMC::calculateWeightEnergyScanCloserPz(nuEnergy, MET, lepton);
    weight = weight_res.at(1);
    if (not TMath::IsNaN(m) && not TMath::IsNaN(weight)) {
      histo->Fill(m/1000., weight);
    }
  }
  weightedMean = histo->GetMean();
  delete histo;
  return weightedMean;
}

// FUNCTIONS

double MMC::mCollProj(TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  TVector2 lep2(lepton.X(), lepton.Y());
  double x = lep2.Mod2()/(lep2.Mod2() + MET*lep2);
  return TMath::Sqrt(2*lepton*lepFV/x);
}

double MMC::mCollATLAS(TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  TVector2 lep2(lepton.X(), lepton.Y());
  double x = lep2.Mod2()/(lep2.Mod2() + MET.Mod()*lep2.Mod());
  return TMath::Sqrt(2*lepton*lepFV/x);
}

double MMC::calculateMassEnergyScanCloserPz(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET) {
  double mass = 0;
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  if (TMath::IsNaN(pzplus) && TMath::IsNaN(pzminus)) {
    return pzplus;
  }
  else {
    if (TMath::Abs(pzplus - lepton.Z()) < TMath::Abs(pzminus - lepton.Z())) {
      pz = pzplus;
    }
    else if (TMath::Abs(pzplus - lepton.Z()) > TMath::Abs(pzminus - lepton.Z())) {
      pz = pzminus;
    }
    TLorentzVector nuSystem = MMC::nuFourVector(MET, pz, nuEnergy);
    TLorentzVector higgs = nuSystem + lepton + higgsLepton;
    mass = higgs.M();
    return mass;
  }
}

double MMC::calculateMassEnergyScanSameSignPz(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET) {
  double mass = 0;
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  TLorentzVector nuSystemPlus = MMC::nuFourVector(MET, pzplus, nuEnergy);
  TLorentzVector higgsPlus = nuSystemPlus + lepton + higgsLepton;
  TLorentzVector nuSystemMinus = MMC::nuFourVector(MET, pzminus, nuEnergy);
  TLorentzVector higgsMinus = nuSystemMinus + lepton + higgsLepton;
  if (TMath::IsNaN(pzplus) && TMath::IsNaN(pzminus)) {
    return pzplus;
  }
  else {
    if (TMath::Sign(1, pzplus) == TMath::Sign(1, lepton.Z())) {
      mass = higgsPlus.M();
    }
    else if (TMath::Sign(1, pzminus) == TMath::Sign(1, lepton.Z())) {
      mass = higgsMinus.M();
    }
    else {
      // average over both solutions
      mass = (higgsPlus.M() + higgsMinus.M())/2.;
    }
    return mass;
  }
}

// create fourvector for the neutrino system
TLorentzVector MMC::nuFourVector(TVector2 nuTrans, double pz, double nuEnergy) {
  TLorentzVector nuFour;
  nuFour.SetPxPyPzE(nuTrans.X(), nuTrans.Y(), pz, nuEnergy);
  return nuFour;
}

double MMC::calculateWeightMassScan(double mNu) {
  // set weight to zero if not between 0 and m_tau
  if (mNu < 0 || mNu > tauMass) {
    return 0;
  }
  else {
    double weight = (1 - 3*TMath::Power(mNu/tauMass, 4) + 2*TMath::Power(mNu/tauMass, 6));
    // weight of mNu
    weight *= 4*mNu/TMath::Power(tauMass, 2);
    //  // weight of mNu^2
    //  weight *= 35./24./tauMass;
    return weight;
  }
}

double MMC::fittedMassWeight(double mNu) {
  if (mNu < 0 || mNu > tauMass) {
    return 0;
  }
  else {
    double p0 = 38.4168;
    double p1 = 3627.46;
    double p2 = 26829.4;
    double p3 = -102632;
    double p4 = 181296;
    double p5 = -164887;
    double p6 = 71782.9;
    double p7 = -11824.6;
    double weight = p0 + p1*mNu + p2*TMath::Power(mNu, 2) + p3*TMath::Power(mNu, 3) + p4*TMath::Power(mNu, 4);
    weight +=  p5*TMath::Power(mNu, 5) + p6*TMath::Power(mNu, 6) + p7*TMath::Power(mNu, 7);
    return weight;
  }
}

// must return a vector containing two entries
vector<double> MMC::calculateWeightEnergyScanCloserPzVector(double nuEnergy, TVector2 MET, TLorentzVector lepton) {
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  double jacobi = 0;
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  vector<double> ret;
  if (TMath::IsNaN(pzplus) && TMath::IsNaN(pzminus)) {
    return ret;
  }
  else {
    if (TMath::Abs(pzplus - lepton.Z()) < TMath::Abs(pzminus - lepton.Z())) {
      pz = pzplus;
      jacobi = (nuEnergy - 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
    }
    else if (TMath::Abs(pzplus - lepton.Z()) > TMath::Abs(pzminus - lepton.Z())) {
      pz = pzminus;
      jacobi = (nuEnergy + 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
    }
    double mNu = TMath::Sqrt(TMath::Power(nuEnergy, 2) - MET.Mod2() - TMath::Power(pz, 2));
    jacobi /= mNu;
    if (TMath::IsNaN(mNu)) {
      return ret;
    }
    if (mNu < 0 || mNu > tauMass) {
      return ret;
    }
    else {
      double weight = 4*mNu/TMath::Power(tauMass, 2);
      weight *= (1 - 3*TMath::Power(mNu/tauMass, 4) + 2*TMath::Power(mNu/tauMass, 6));
//      Faktor wegen Jacobi-Determinante -> Gewicht dadurch nicht mehr auf 1 normiert
      weight /= TMath::Abs(jacobi);
      if (weight > 1) {
//        cout << weight << " " << mNu << " " << nuEnergy << " " << pz << " " << jacobi << endl;
      }
      ret.push_back(weight);
      ret.push_back(mNu);
      return ret;
    }
  }
}

vector<double> MMC::calculateWeightEnergyScanCloserPz(double nuEnergy, TVector2 MET, TLorentzVector lepton) {
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  double jacobi = 0;
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  vector<double> ret;
  if (TMath::Abs(pzplus - lepton.Z()) < TMath::Abs(pzminus - lepton.Z())) {
    pz = pzplus;
    jacobi = (nuEnergy - 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
  }
  else if (TMath::Abs(pzplus - lepton.Z()) > TMath::Abs(pzminus - lepton.Z())) {
    pz = pzminus;
    jacobi = (nuEnergy + 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
  }
  // kein else?
  double mNu = TMath::Sqrt(TMath::Power(nuEnergy, 2) - MET.Mod2() - TMath::Power(pz, 2));
  jacobi /= mNu;
  double weight = 4*mNu/TMath::Power(tauMass, 2);
  weight *= (1 - 3*TMath::Power(mNu/tauMass, 4) + 2*TMath::Power(mNu/tauMass, 6));
  // Faktor wegen Jacobi-Determinante -> Gewicht dadurch nicht mehr auf 1 normiert
  weight /= TMath::Abs(jacobi);
  ret.push_back(pz);
  ret.push_back(weight);
  ret.push_back(mNu);
  return ret;
}

vector<double> MMC::calculateWeightEnergyScanSameSignPz(double nuEnergy, TVector2 MET, TLorentzVector lepton) {
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  double jacobi = 0;
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  vector<double> ret;
  if (TMath::Sign(1, pzplus) == TMath::Sign(1, lepton.Z())) {
    pz = pzplus;
    jacobi = (nuEnergy - 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
  }
  else if (TMath::Sign(1, pzminus) == TMath::Sign(1, lepton.Z())) {
    pz = pzminus;
    jacobi = (nuEnergy + 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
  }
  // kein else?
  double mNu = TMath::Sqrt(TMath::Power(nuEnergy, 2) - MET.Mod2() - TMath::Power(pz, 2));
  jacobi /= mNu;
  double weight = 4*mNu/TMath::Power(tauMass, 2);
  weight *= (1 - 3*TMath::Power(mNu/tauMass, 4) + 2*TMath::Power(mNu/tauMass, 6));
  // Faktor wegen Jacobi-Determinante -> Gewicht dadurch nicht mehr auf 1 normiert
  weight /= TMath::Abs(jacobi);
  ret.push_back(pz);
  ret.push_back(weight);
  ret.push_back(mNu);
  return ret;
}

vector<double> MMC::calculateWeightEnergyScanSameSignPzVector(double nuEnergy, TVector2 MET, TLorentzVector lepton) {
  double pz = 0;
  double pzplus = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  double pzminus = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  double jacobi = 0;
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  vector<double> ret;
  if (TMath::IsNaN(pzplus) && TMath::IsNaN(pzminus)) {
    return ret;
  }
  else {
    if (TMath::Sign(1, pzplus) == TMath::Sign(1, lepton.Z())) {
      pz = pzplus;
      jacobi = (nuEnergy - 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
    }
    else if (TMath::Sign(1, pzminus) == TMath::Sign(1, lepton.Z())) {
      pz = pzminus;
      jacobi = (nuEnergy + 2*pz*(lepton.E()+nuEnergy)/TMath::Sqrt(TMath::Power(2*lepton.Z(), 2) - 4*c));
    }
    double mNu = TMath::Sqrt(TMath::Power(nuEnergy, 2) - MET.Mod2() - TMath::Power(pz, 2));
    jacobi /= mNu;
    if (TMath::IsNaN(mNu) || mNu < 0 || mNu > tauMass) {
      return ret;
    }
    else {
      double weight = 4*mNu/TMath::Power(tauMass, 2);
      weight *= (1 - 3*TMath::Power(mNu/tauMass, 4) + 2*TMath::Power(mNu/tauMass, 6));
//      Faktor wegen Jacobi-Determinante -> Gewicht dadurch nicht mehr auf 1 normiert
      weight /= TMath::Abs(jacobi);
      if (weight > 1) {
//        cout << weight << " " << mNu << " " << nuEnergy << " " << pz << " " << jacobi << endl;
      }
      ret.push_back(weight);
      ret.push_back(mNu);
      return ret;
    }
  }
}

// PLUS
// calculate Higgs mass from Fourvectors
double MMC::calculateMassEnergyScanPlus(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET) {
  double mass = 0;
  double pz = MMC::nuZComponentPlus(lepton, MET, nuEnergy);
  if (TMath::IsNaN(pz)) {
    return pz;
  }
  else {
    TLorentzVector nuSystem = MMC::nuFourVector(MET, pz, nuEnergy);
    TLorentzVector higgs = nuSystem + lepton + higgsLepton;
    mass = higgs.M();
    return mass;
  }
}

// calculate the z-component of the fourvector of the neutrino system
double MMC::nuZComponentPlus(TLorentzVector lepton, TVector2 MET, double nuEnergy) {
  // use formula for quadratic equations of the form ax^2 + bx + c = 0
  double b = 2*lepton.Z();
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  double pz = (- b + TMath::Sqrt(TMath::Power(b, 2) - 4*c))/2;
  return pz;
}

// MINUS
// calculate Higgs mass from Fourvectors
double MMC::calculateMassEnergyScanMinus(double nuEnergy, TLorentzVector lepton, TLorentzVector higgsLepton, TVector2 MET) {
  double mass = 0;
  double pz = MMC::nuZComponentMinus(lepton, MET, nuEnergy);
  if (TMath::IsNaN(pz)) {
    return pz;
  }
  else {
    TLorentzVector nuSystem = MMC::nuFourVector(MET, pz, nuEnergy);
    TLorentzVector higgs = nuSystem + lepton + higgsLepton;
    mass = higgs.M();
    return mass;
  }
}

// calculate the z-component of the fourvector of the neutrino system
double MMC::nuZComponentMinus(TLorentzVector lepton, TVector2 MET, double nuEnergy) {
  // use formula for quadratic equations of the form ax^2 + bx + c = 0
  double b = 2*lepton.Z();
  double c = 2*(lepton.X()*MET.X() + lepton.Y()*MET.Y() - nuEnergy*lepton.E());
  c += MET.Mod2() - TMath::Power(nuEnergy, 2) - lepton.M2() + TMath::Power(tauMass, 2);
  // only considering "+"-case first
  double pz = (- b - TMath::Sqrt(TMath::Power(b, 2) - 4*c))/2;
  return pz;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// SCAN OVER M_NU
////////////////////////////////////////////////////////////////////////////////////////////////////

// calculate pNuZ
double MMC::pNuZPlus(TVector2 MET, TLorentzVector lepton, double mNu) {
  double d = MET.X()*lepton.X() + MET.Y()*lepton.Y() + 0.5*(TMath::Power(tauMass, 2) - TMath::Power(mNu, 2) - lepton.M2());
  double a = TMath::Power(lepton.E(), 2) - TMath::Power(lepton.Z(), 2);
  double b = -2*lepton.Z()*d;
  double c = TMath::Power(lepton.E(), 2)*(MET.Mod2() + TMath::Power(mNu, 2)) - TMath::Power(d, 2);
  double pz = (- b + TMath::Sqrt(TMath::Power(b, 2) - 4*a*c))/2/a;
  return pz;
}

double MMC::pNuZMinus(TVector2 MET, TLorentzVector lepton, double mNu) {
  double d = MET.X()*lepton.X() + MET.Y()*lepton.Y() + 0.5*(TMath::Power(tauMass, 2) - TMath::Power(mNu, 2) - lepton.M2());
  double a = TMath::Power(lepton.E(), 2) - TMath::Power(lepton.Z(), 2);
  double b = -2*lepton.Z()*d;
  double c = TMath::Power(lepton.E(), 2)*(MET.Mod2() + TMath::Power(mNu, 2)) - TMath::Power(d, 2);
  double pz = (- b - TMath::Sqrt(TMath::Power(b, 2) - 4*a*c))/2/a;
  return pz;
}

double MMC::nuEnergy(TVector2 MET, double pNuZ, double mNu) {
  double E = TMath::Sqrt(MET.Mod2() + TMath::Power(pNuZ, 2) + TMath::Power(mNu, 2));
  return E;
}

double MMC::calculateMassNuMScan(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  double mass = 0;
  double pzplus = MMC::pNuZPlus(MET, lepton, mNu);
  double pzminus = MMC::pNuZMinus(MET, lepton, mNu);
  TLorentzVector nuSystemP, nuSystemM;
  nuSystemP.SetXYZM(MET.X(), MET.Y(), pzplus, mNu);
  nuSystemM.SetXYZM(MET.X(), MET.Y(), pzminus, mNu);
  TLorentzVector higgsP = nuSystemP + lepton + lepFV;
  double massP = higgsP.M();
  TLorentzVector higgsM = nuSystemM + lepton + lepFV;
  double massM = higgsM.M();
  // if (TMath::Abs(massP - 125000) < TMath::Abs(massM - 125000)) {
  //   mass = massP;
  // }
  // else if (TMath::Abs(massP - 125000) > TMath::Abs(massM - 125000)) {
  //   mass = massM;
  // }
  mass = (massP + massM)/2.;
  return mass;
}

double MMC::calculateMassNuMScanPlus(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  double mass = 0;
  double pz = MMC::pNuZPlus(MET, lepton, mNu);
  double nuE = MMC::nuEnergy(MET, pz, mNu);
  TLorentzVector nuSystem = MMC::nuFourVector(MET, pz, nuE);
  TLorentzVector higgs = nuSystem + lepton + lepFV;
  mass = higgs.M();
  return mass;
}

double MMC::calculateMassNuMScanMinus(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  double mass = 0;
  double pz = MMC::pNuZMinus(MET, lepton, mNu);
  double nuE = MMC::nuEnergy(MET, pz, mNu);
  TLorentzVector nuSystem = MMC::nuFourVector(MET, pz, nuE);
  TLorentzVector higgs = nuSystem + lepton + lepFV;
  mass = higgs.M();
  return mass;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// SCAN OVER M_NU BUT FOUR SOLUTIONS --> MORE COMPLICATED THAN THE OTHER TWO SOLUTIONS
////////////////////////////////////////////////////////////////////////////////////////////////////

// two solutions for plus and minus in the quadratic equation
double MMC::nuSystemEnergyPlus(TVector2 MET, TLorentzVector lepton,  double mNu) {
  double delta = TMath::Power(mNu, 2) + lepton.M2() - TMath::Power(tauMass, 2) - 2*(MET.X()*lepton.X() + MET.Y()*lepton.Y());
  double alpha = TMath::Power(2*lepton.Z(), 2) - 4*TMath::Power(lepton.E(), 2);
  double beta = -4*lepton.E()*delta;
  double gamma = -TMath::Power(mNu, 2) - MET.Mod2() - TMath::Power(delta, 2);
  double nuE = (- beta + TMath::Sqrt(TMath::Power(beta, 2) - 4*alpha*gamma))/2/alpha;
  return nuE;
}

double MMC::nuSystemEnergyMinus(TVector2 MET, TLorentzVector lepton,  double mNu) {
  double delta = TMath::Power(mNu, 2) + lepton.M2() - TMath::Power(tauMass, 2) - 2*(MET.X()*lepton.X() + MET.Y()*lepton.Y());
  double alpha = TMath::Power(2*lepton.Z(), 2) - 4*TMath::Power(lepton.E(), 2);
  double beta = -4*lepton.E()*delta;
  double gamma = -TMath::Power(mNu, 2) - MET.Mod2() - TMath::Power(delta, 2);
  double nuE = (- beta - TMath::Sqrt(TMath::Power(beta, 2) - 4*alpha*gamma))/2/alpha;
  return nuE;
}

// two solutions for plus and minus because of squared equation
double MMC::nuZUsingNuMassPlus(TVector2 MET, double nuM, double nuE) {
  double pz = TMath::Sqrt(TMath::Power(nuE, 2) - TMath::Power(nuM, 2) - MET.Mod2());
  return pz;
}

double MMC::nuZUsingNuMassMinus(TVector2 MET, double nuM, double nuE) {
  double pz = -TMath::Sqrt(TMath::Power(nuE, 2) - TMath::Power(nuM, 2) - MET.Mod2());
  return pz;
}

// only one solution out of four possible ones
double MMC::calculateMassScanningM(double mNu, TLorentzVector lepton, TLorentzVector lepFV, TVector2 MET) {
  double nuEP = MMC::nuSystemEnergyPlus(MET, lepton, mNu);
  double nuEM = MMC::nuSystemEnergyMinus(MET, lepton, mNu);
  double pzPP = MMC::nuZUsingNuMassPlus(MET, mNu, nuEP);
  double pzPM = MMC::nuZUsingNuMassMinus(MET, mNu, nuEP);
  double pzMP = MMC::nuZUsingNuMassPlus(MET, mNu, nuEM);
  double pzMM = MMC::nuZUsingNuMassMinus(MET, mNu, nuEM);
  // double pzP, pzM, nuZ, nuE;

  // if (TMath::Abs(pzPP - lepton.Z()) < TMath::Abs(pzPM - lepton.Z())) {
  //   pzP = pzPP;
  // }
  // else if (TMath::Abs(pzPP - lepton.Z()) > TMath::Abs(pzPM - lepton.Z())) {
  //   pzP = pzPM;
  // }
  // if (TMath::Abs(pzMP - lepton.Z()) < TMath::Abs(pzMM - lepton.Z())) {
  //   pzM = pzMP;
  // }
  // else if (TMath::Abs(pzMP - lepton.Z()) > TMath::Abs(pzMM - lepton.Z())) {
  //   pzM = pzMM;
  // }
  // if (TMath::Abs(pzP - lepton.Z()) < TMath::Abs(pzM - lepton.Z())) {
  //   nuZ = pzP;
  //   nuE = nuEP;
  // }
  // else if (TMath::Abs(pzP - lepton.Z()) > TMath::Abs(pzM - lepton.Z())) {
  //   nuZ = pzM;
  //   nuE = nuEM;
  // }

  // double nuE = MMC::nuSystemEnergyPlus(MET, lepton, mNu);
  // double nuZ = MMC::nuZUsingNuMass(MET, mNu, nuE);
  // TLorentzVector nuSystem = MMC::nuFourVector(MET, nuZ, nuE);

  TLorentzVector nuSystemPP = MMC::nuFourVector(MET, pzPP, nuEP);
  TLorentzVector nuSystemPM = MMC::nuFourVector(MET, pzPM, nuEP);
  TLorentzVector nuSystemMP = MMC::nuFourVector(MET, pzMP, nuEM);
  TLorentzVector nuSystemMM = MMC::nuFourVector(MET, pzMM, nuEM);
  TLorentzVector nuSystem = nuSystemPP;
  if (TMath::Abs(nuSystem.M() - mNu) > TMath::Abs(nuSystemPM.M() - mNu)) {
    nuSystem = nuSystemPM;
  }
  if (TMath::Abs(nuSystem.M() - mNu) > TMath::Abs(nuSystemMP.M() - mNu)) {
    nuSystem = nuSystemMP;
  }
  if (TMath::Abs(nuSystem.M() - mNu) > TMath::Abs(nuSystemMM.M() - mNu)) {
    nuSystem = nuSystemMM;
  }
  TLorentzVector higgs = nuSystem + lepton + lepFV;
  double mass = higgs.M();
  return mass;
}

double MMC::fLimit(double phiLepFV, double phiLepton) {
  double a = 92.5;
  double ret = -a*(phiLepFV - phiLepton);
  return ret;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// NOT NEEDED YET
////////////////////////////////////////////////////////////////////////////////////////////////////
// because neutrino energy is being scanned

// calculate the energy of the lepton in the Tau restframe
double MMC::leptonRestEnergy(TLorentzVector tau, TLorentzVector lepton, TLorentzVector nu) {
  double E = 0;
  E += TMath::Power(tau.M(), 2);
  E += TMath::Power(lepton.M(), 2);
  E -= TMath::Power(nu.M(), 2);
  E /= 2*tau.M();
  return E;
}

// calculate the energy of the tau in the lab frame
double MMC::tauLabEnergy(TLorentzVector tau, double leptonRestE, double leptonLabE) {
  double E = tau.M()/2*(leptonLabE/leptonRestE - leptonRestE/leptonLabE);
  return E;
}

// calculate the energy of the nu-system in the lab frame
double MMC::nuLabEnergy(double tauE, double leptonE) {
  double E = tauE - leptonE;
  return E;
}
